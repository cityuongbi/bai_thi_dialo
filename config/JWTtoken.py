from datetime import datetime, timedelta
from jose import jwt, JWTError
from models.token_model import TokenData
from typing import Optional

SECRET_KEY = "ToiYeuPTIT"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("email")
        username: str = payload.get("username")
        user_id: str = payload.get("user_id")
        permissions: str = payload.get("permissions")
        is_super_admin: bool = payload.get("is_super_admin")
        roles: list[str] = payload.get("roles")
        tenant_id: str = payload.get("tenant_id")
        if email is None:
            raise credentials_exception

        token_data = TokenData(email=email, username=username, user_id=user_id, permissions=permissions,
                               is_super_admin=is_super_admin, roles=roles, tenant_id=tenant_id)

        return token_data
    except JWTError:
        raise credentials_exception
