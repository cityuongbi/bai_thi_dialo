from datetime import datetime
from routers.authentication_router import get_current_user
from models.user_model import UserPermission
from bson import ObjectId
from fastapi import APIRouter, Depends
from models.permission_model import HasPermission
from models.user_model import User
from schemas import user_schema
from config.hashing import Hash

router = APIRouter(
    prefix='/user',
    tags=['User']
)


def users_serializer(users):
    return [{
        'username': user['username'],
        'email': user['email'],
        'fullname': user['fullname'],
        'is_active': user['is_active'],
        'is_super_admin': user['is_super_admin'],
        'unit': user['unit'],
        'department': user['department'],
        'position': user['position'],
        'phone': user['phone'],
        'tenant_id': user['tenant_id'],
        'employee_id': user['employee_id'],
        'created_at': user['created_at'],
        'updated_at': user['updated_at'],
        'deleted_at': user['deleted_at']
    } for user in users]


@router.get('/get_users')
async def get_users(authorize: bool = Depends(HasPermission(['Xem danh sách user'])),
                    current_user: UserPermission = Depends(get_current_user)):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    users = await User.find({'tenant_id': current_user.tenant_id, 'deleted_at': None}).to_list(None)

    return {
        'status': 200,
        'data': users_serializer(users)
    }


@router.put('/update_user/{user_id}')
async def update_user(user_id: str, new_info: user_schema.UserUpdate,
                      authorize: bool = Depends(HasPermission(['Cập nhật thông tin user']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    user = await User.find_one({'_id': ObjectId(user_id), 'deleted_at': None})

    if not user:
        return {
            'status': 404,
            'data': 'Không tìm thấy user!'
        }

    for field, value in new_info.dict().items():
        if value is None:
            continue
        if field == 'password':
            value = Hash.bcrypt(value)
        user[field] = value

    await user.commit()

    return {
        'status': 200,
        'data': 'Cập nhật thông tin user thành công!'
    }


@router.delete('/delete_user/{user_id}')
async def delete_user(user_id: str, authorize: bool = Depends(HasPermission(['Xóa user']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    user = await User.find_one({'_id': ObjectId(user_id), 'deleted_at': None})

    if not user:
        return {
            'status': 404,
            'data': 'Không tìm thấy user!'
        }

    user.deleted_at = str(datetime.utcnow())
    user.updated_at = str(datetime.utcnow())

    await user.commit()

    return {
        'status': 200,
        'data': 'Xóa user thành công!'
    }


@router.put('/update_is_super_admin')
async def update_is_super_admin(new_info: user_schema.SuperAdminUpdate,
                          authorize: bool = Depends(HasPermission(['Cập nhật super admin']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    user = await User.find_one({'_id': ObjectId(new_info.user_id), 'deleted_at': None})

    if not user:
        return {
            'status': 404,
            'data': 'Không tìm thấy user!'
        }

    user.is_super_admin = new_info.is_super_admin

    await user.commit()

    return {
        'status': 200,
        'data': 'Cập nhật super admin thành công!'
    }


