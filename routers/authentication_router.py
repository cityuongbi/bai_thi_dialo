from fastapi import APIRouter, Depends, status, HTTPException
from config import JWTtoken
from config.JWTtoken import ACCESS_TOKEN_EXPIRE_MINUTES, create_access_token
from config.hashing import Hash
from models.role_permission_model import RolePermission
from models.user_model import User
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from datetime import timedelta
from schemas import user_schema
from models.user_role_model import UserRole

router = APIRouter(
    prefix='/auth',
    tags=['Authentication']
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/login")


def serialize_user_role(user_role):
    return {
        'user_id': str(user_role.user_id),
        'role_id': str(user_role.role_id)
    }


@router.post('/new_user')
async def create_user(user: user_schema.User):
    existing_user = await User.find_one({'$or': [{'username': user.username}, {'email': user.email}]})

    if existing_user:
        return {
            'status_code': '203',
            'message': 'Người dùng đã tồn tại.'
        }

    new_user = User(**user.dict())
    new_user.password = Hash.bcrypt(new_user.password)

    await new_user.commit()

    return {
        'status_code': '200',
        'message': 'Thêm user thành công.'
    }


@router.post('/login')
async def login(user: OAuth2PasswordRequestForm = Depends()):
    this_user = await User.find_one({'username': user.username, 'deleted_at': None, 'is_active': True})

    if not this_user or not Hash.verify(user.password, this_user['password']):
        return {
            'status_code': '203',
            'message': 'Thông tin đăng nhập không chính xác.'
        }

    permission_ids = []
    role_ids = []

    user_roles = await UserRole.find({'user_id': str(this_user.id), 'deleted_at': None}).to_list(length=None)

    if user_roles:
        for user_role in user_roles:
            user_role = serialize_user_role(user_role)
            role_ids.append(user_role['role_id'])
            role_permissions = await RolePermission.find({"role_id": user_role['role_id'], 'deleted_at': None}).to_list(length=None)
            for permission in role_permissions:
                permission_ids.append(str(permission['permission_id']))

    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"email": this_user.email, "username": this_user.username, "user_id": str(this_user.id),
              'roles': role_ids, 'permissions': permission_ids, 'is_super_admin': this_user.is_super_admin,
              'tenant_id': this_user.tenant_id},
        expires_delta=access_token_expires
    )

    return {
        "access_token": access_token,
        "token_type": "bearer"
    }


@router.get('/get_current_user')
def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Thông tin đăng nhập không chính xác.",
        headers={"WWW-Authenticate": "Bearer"},
    )
    return JWTtoken.verify_token(token, credentials_exception)
