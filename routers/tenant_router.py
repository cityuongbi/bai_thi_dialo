from bson import ObjectId
from fastapi import APIRouter, Depends
from models.permission_model import HasPermission
from models.tenant_model import Tenant
from schemas import tenant_schema

router = APIRouter(
    prefix='/tenant',
    tags=['Tenant']
)


def tenants_serializer(tenants):
    return [{
        'name': tenant['name'],
        'created_at': tenant['created_at'],
        'updated_at': tenant['updated_at'],
        'deleted_at': tenant['deleted_at']
    } for tenant in tenants]


@router.post('/create_tenant')
async def create_tenant(tenant: tenant_schema.Tenant,
                        authorize: bool = Depends(HasPermission(['Tạo tenant mới']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    tenant_exist = await Tenant.find_one({'name': tenant.name, 'deleted_at': None})

    if tenant_exist:
        return {
            'status': 404,
            'data': 'Tenant đã tồn tại!'
        }

    new_tenant = Tenant(**tenant.dict())

    await new_tenant.commit()

    return {
        'status': 200,
        'data': 'Tạo tenant thành công!'
    }


@router.get('/get_tenants')
async def get_tenants(authorize: bool = Depends(HasPermission(['Xem danh sách tenant']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    tenants = await Tenant.find({'deleted_at': None}).to_list(None)

    return {
        'status': 200,
        'data': tenants_serializer(tenants)
    }


@router.put('/update_tenant/{tenant_id}')
async def update_tenant(tenant_id: str, new_info: tenant_schema.Tenant,
                        authorize: bool = Depends(HasPermission(['Cập nhật thông tin tenant']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    tenant = await Tenant.find_one({'_id': ObjectId(tenant_id), 'deleted_at': None})

    if not tenant:
        return {
            'status': 404,
            'data': 'Tenant không tồn tại!'
        }

    for field, value in new_info.dict().items():
        if value is not None:
            tenant[field] = value

    await tenant.commit()

    return {
        'status': 200,
        'data': 'Cập nhật thông tin tenant thành công!'
    }