from datetime import datetime

from bson import ObjectId
from fastapi import APIRouter, Depends
from schemas import permission_schema
from models.permission_model import Permission, HasPermission

router = APIRouter(
    prefix='/permission',
    tags=['Permission']
)


@router.post('/create_permission')
async def create_permission(permission: permission_schema.Permission,
                            authorize: bool = Depends(HasPermission(['Tạo permission mới']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    permission_exist = await Permission.find_one({'name': permission.name}, {'tenant_id': permission.tenant_id})

    if permission_exist:
        return {
            'status': 400,
            'data': 'Permission đã tồn tại!'
        }

    new_permission = Permission(**permission.dict())

    await new_permission.commit()

    return {
        'status': 200,
        'data': 'Thêm permission thành công!'
    }


@router.put('/update_permission/{permission_id}')
async def update_permission(permission_id: str, new_info: permission_schema.PermissionUpdate,
                            authorize: bool = Depends(HasPermission(['Cập nhật thông tin permission']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    permission = await Permission.find_one({'_id': ObjectId(permission_id), 'deleted_at': None})

    if not permission:
        return {
            'status': 404,
            'data': 'Permission không tồn tại!'
        }

    for field, value in new_info.dict().items():
        if value is None:
            continue
        permission[field] = value

    await permission.commit()

    return {
        'status': 200,
        'data': 'Cập nhật thông tin permission thành công!'
    }


@router.delete('/delete_permission/{permission_id}')
async def delete_permission(permission_id: str,
                            authorize: bool = Depends(HasPermission(['Xóa permission']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    permission = await Permission.find_one({'_id': ObjectId(permission_id), 'deleted_at': None})

    if not permission:
        return {
            'status': 404,
            'data': 'Permission không tồn tại!'
        }

    permission.deleted_at = str(datetime.utcnow())
    permission.updated_at = str(datetime.utcnow())

    await permission.commit()

    return {
        'status': 200,
        'data': 'Xóa permission thành công!'
    }