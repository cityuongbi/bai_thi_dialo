from datetime import datetime
from bson import ObjectId
from fastapi import APIRouter, Depends
from models.role_permission_model import RolePermission
from models.user_model import User
from schemas import role_schema
from models.role_model import Role
from models.permission_model import HasPermission, Permission
from schemas import role_permission_schema, user_role_schema
from models.user_role_model import UserRole
from routers.authentication_router import get_current_user
from models.user_model import UserPermission

router = APIRouter(
    prefix='/role',
    tags=['Role']
)


def role_serializer(role):
    return {
        'name': role['name'],
        'tenant_id': role['tenant_id'],
        'created_at': role['created_at'],
        'updated_at': role['updated_at'],
        'deleted_at': role['deleted_at']
    }


def roles_serializer(roles):
    return [role_serializer(role) for role in roles]


def permissions_serializer(permission):
    return {
        'name': permission['name'],
        'tenant_id': permission['tenant_id'],
        'created_at': permission['created_at'],
        'updated_at': permission['updated_at'],
        'deleted_at': permission['deleted_at']
    }


@router.post('/create_role')
async def create_role(role: role_schema.Role,
                      authorize: bool = Depends(HasPermission(['Tạo role mới']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role_exist = await Role.find_one({'name': role.name, 'tenant_id': role.tenant_id, 'deleted_at': None})

    if role_exist:
        return {
            'status': 404,
            'data': 'Role đã tồn tại!'
        }

    new_role = Role(**role.dict())

    await new_role.commit()

    return {
        'status': 200,
        'data': 'Thêm role thành công!'
    }


@router.get('/get_roles')
async def get_roles(authorize: bool = Depends(HasPermission(['Xem tất cả các roles'])),
                    current_user: UserPermission = Depends(get_current_user)):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    roles = await Role.find({'tenant_id': current_user.tenant_id, 'deleted_at': None}).to_list(None)

    return {
        'status': 200,
        'data': roles_serializer(roles)
    }


@router.post('/add_permission_to_role')
async def add_permission_to_role(role_permission: role_permission_schema.RolePermission,
                                 authorize: bool = Depends(HasPermission(['Thêm permissions cho role']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role = await Role.find_one({'_id': ObjectId(role_permission.role_id), 'deleted_at': None})

    if not role:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    for permission_id in role_permission.permission_ids:
        new_role_permission = RolePermission(role_id=role_permission.role_id, permission_id=permission_id,
                                             created_at=role_permission.created_at,
                                             updated_at=str(datetime.now()),
                                             deleted_at=role_permission.deleted_at)
        await new_role_permission.commit()

    return {
        'status': 200,
        'data': 'Thêm permissions vào role thành công!'
    }


@router.post('/add_role_to_user')
async def add_role_to_user(user_role: user_role_schema.UserRole,
                           authorize: bool = Depends(HasPermission(['Thêm role cho user']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role = await Role.find_one({'_id': ObjectId(user_role.role_id), 'deleted_at': None})

    if not role:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    user = await User.find_one({'_id': ObjectId(user_role.user_id)})

    if not user:
        return {
            'status': 404,
            'data': 'User không tồn tại!'
        }

    new_user_role = UserRole(**user_role.dict())
    new_user_role['updated_at'] = (str(datetime.now()))

    await new_user_role.commit()

    return {
        'status': 200,
        'data': 'Thêm role vào user thành công!'
    }


@router.delete('/delete_role')
async def delete_role(role_id: str, authorize: bool = Depends(HasPermission(['Xóa role']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role = await Role.find_one({'_id': ObjectId(role_id), 'deleted_at': None})

    if not role:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    role['deleted_at'] = (str(datetime.now()))
    role['updated_at'] = (str(datetime.now()))

    await role.commit()

    return {
        'status': 200,
        'data': 'Xóa role thành công!'
    }


@router.put('/restore_role')
async def restore_role(role_id: str, authorize: bool = Depends(HasPermission(['Khôi phục role']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role = await Role.find_one({'_id': ObjectId(role_id), 'deleted_at': {'$ne': None}})

    if not role:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    role['deleted_at'] = None

    await role.commit()

    return {
        'status': 200,
        'data': 'Khôi phục role thành công!'
    }


@router.get('/get_role_permissions')
async def get_role_permissions(role_id: str,
                               authorize: bool = Depends(HasPermission(['Xem permissions của role']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role = await Role.find_one({'_id': ObjectId(role_id), 'deleted_at': None})

    if not role:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    permission_ids = await RolePermission.find({'role_id': role_id, 'deleted_at': None}).to_list(None)

    permissions = []

    for permission_id in permission_ids:
        permission = await Permission.find_one({'_id': ObjectId(permission_id.permission_id), 'deleted_at': None})
        permissions.append(permissions_serializer(permission))

    return {
        'status': 200,
        'data': permissions
    }


@router.put('/update_role')
async def update_role(role: role_schema.RoleUpdate, authorize: bool = Depends(HasPermission(['Sửa role']))):
    if not authorize:
        return {
            'status': 401,
            'data': 'Bạn không có quyền truy cập.'
        }

    role_update = await Role.find_one({'_id': ObjectId(role.id), 'deleted_at': None})

    if not role_update:
        return {
            'status': 404,
            'data': 'Role không tồn tại!'
        }

    for field, value in role.dict().items():
        if value is not None:
            role_update[field] = value
