from datetime import datetime
from typing import Optional
from pydantic import BaseModel, EmailStr, validator


class User(BaseModel):
    username: str
    password: str
    email: EmailStr
    fullname: str
    is_active: bool
    is_super_admin: bool
    unit: Optional[str] = None
    department: str
    position: str
    phone: Optional[str] = None
    tenant_id: str
    employee_id: str
    created_at: Optional[str] = None
    updated_at: Optional[str] = None
    deleted_at: Optional[str] = None

    @validator('phone')
    def phone_valid(cls, phone):
        if phone is None:
            return phone
        if not phone.startswith('0') or len(phone) != 10 or not phone[1:].isdigit():
            raise ValueError("Số điện thoại không hợp lệ.")
        return phone

    @validator('created_at', pre=True, always=True)
    def validate_created_at(cls, created_at):
        if created_at is None:
            created_at = datetime.now().isoformat()
        try:
            datetime.fromisoformat(created_at)
        except ValueError:
            raise ValueError("Ngày tạo không hợp lệ.")
        return created_at

    @validator('updated_at', pre=True, always=True)
    def validate_updated_at(cls, updated_at):
        if updated_at is None:
            updated_at = datetime.now().isoformat()
        try:
            datetime.fromisoformat(updated_at)
        except ValueError:
            raise ValueError("Ngày cập nhật không hợp lệ.")
        return updated_at

    @validator('deleted_at', pre=True, always=True)
    def validate_deleted_at(cls, deleted_at):
        if deleted_at is None:
            return deleted_at
        try:
            datetime.fromisoformat(deleted_at)
        except ValueError:
            raise ValueError("Ngày xóa không hợp lệ.")
        return deleted_at


class UserUpdate(BaseModel):
    username: Optional[str] = None
    password: Optional[str] = None
    email: Optional[EmailStr] = None
    fullname: Optional[str] = None
    is_active: Optional[bool] = None
    unit: Optional[str] = None
    department: Optional[str] = None
    position: Optional[str] = None
    phone: Optional[str] = None
    tenant_id: Optional[str] = None
    employee_id: Optional[str] = None

    @validator('phone')
    def phone_valid(cls, phone):
        if phone is None:
            return phone
        if not phone.startswith('0') or len(phone) != 10 or not phone[1:].isdigit():
            raise ValueError("Số điện thoại không hợp lệ.")
        return phone


class SuperAdminUpdate(BaseModel):
    user_id: str
    is_super_admin: bool