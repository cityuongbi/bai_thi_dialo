from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator


class Role(BaseModel):
    name: str
    tenant_id: str
    created_at: Optional[str] = None
    updated_at: Optional[str] = None
    deleted_at: Optional[str] = None

    @validator('created_at', pre=True, always=True)
    def validate_created_at(cls, created_at):
        if created_at is None:
            created_at = datetime.now().isoformat()
        try:
            datetime.fromisoformat(created_at)
        except ValueError:
            raise ValueError("Ngày tạo không hợp lệ.")
        return created_at

    @validator('updated_at', pre=True, always=True)
    def validate_updated_at(cls, updated_at):
        if updated_at is None:
            updated_at = datetime.now().isoformat()
        try:
            datetime.fromisoformat(updated_at)
        except ValueError:
            raise ValueError("Ngày cập nhật không hợp lệ.")
        return updated_at

    @validator('deleted_at', pre=True, always=True)
    def validate_deleted_at(cls, deleted_at):
        if deleted_at is None:
            return deleted_at
        try:
            datetime.fromisoformat(deleted_at)
        except ValueError:
            raise ValueError("Ngày xóa không hợp lệ.")
        return deleted_at


class RoleUpdate(BaseModel):
    name: Optional[str] = None
    tenant_id: Optional[str] = None