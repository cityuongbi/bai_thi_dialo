from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from routers.authentication_router import router as AuthenticationRouter
from routers.role_router import router as RoleRouter
from routers.permission_router import router as PermissionRouter
from routers.user_router import router as UserRouter
from routers.tenant_router import router as TenantRouter

app = FastAPI()

app.include_router(AuthenticationRouter)
app.include_router(RoleRouter)
app.include_router(PermissionRouter)
app.include_router(UserRouter)
app.include_router(TenantRouter)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)