from umongo import Document, fields
from config.database import instance


@instance.register
class RolePermission(Document):
    role_id = fields.StringField(required=True)
    permission_id = fields.StringField(required=True)
    created_at = fields.DateTimeField(required=True)
    updated_at = fields.DateTimeField(required=True)
    deleted_at = fields.DateTimeField(allow_none=True)

    class Meta:
        collection_name = 'RolePermissions'
