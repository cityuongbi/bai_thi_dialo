from fastapi import Depends
from umongo import Document, fields
from config.database import instance
from models.user_model import UserPermission
from routers.authentication_router import get_current_user


@instance.register
class Permission(Document):
    name = fields.StringField(required=True, unique=True)
    tenant_id = fields.StringField(required=True)
    created_at = fields.DateTimeField(required=True)
    updated_at = fields.DateTimeField(required=True)
    deleted_at = fields.DateTimeField(allow_none=True)

    class Meta:
        collection_name = 'Permissions'


class HasPermission:
    def __init__(self, required_permissions: list[str]):
        self.required_permissions = required_permissions

    def __call__(self, user: UserPermission = Depends(get_current_user)) -> bool:
        if user.is_super_admin:
            return True

        for r_perm in self.required_permissions:
            permission = Permission.find_one({'name': r_perm, 'deleted_at': None, 'tenant_id': user.tenant_id})
            if not permission:
                return False
        return True



