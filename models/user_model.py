from pydantic import BaseModel
from umongo import Document, fields, validate
from config.database import instance


@instance.register
class User(Document):
    username = fields.StringField(required=True, unique=True)
    password = fields.StringField(required=True, validate=validate.Length(min=6))
    email = fields.EmailField(required=True, unique=True, validate=validate.Email())
    fullname = fields.StringField(required=True)
    is_active = fields.BooleanField(required=True)
    is_super_admin = fields.BooleanField(required=True)
    unit = fields.StringField(required=True)
    department = fields.StringField(required=True)
    position = fields.StringField(required=True)
    phone = fields.StringField(allow_none=True, validate=validate.Regexp(r"^\d{10}$"))
    tenant_id = fields.StringField(allow_none=True)
    employee_id = fields.StringField(required=True, unique=True)
    created_at = fields.DateTimeField(required=True)
    updated_at = fields.DateTimeField(required=True)
    deleted_at = fields.DateTimeField(allow_none=True)

    class Meta:
        collection_name = 'Users'


class UserPermission(BaseModel):
    user_id: str
    username: str
    email: str
    permissions: list[str] = []
    is_super_admin: bool
    tenant_id: str

