from umongo import Document, fields
from config.database import instance


@instance.register
class Tenant(Document):
    name = fields.StringField(required=True, unique=True)
    created_at = fields.DateTimeField(required=True)
    updated_at = fields.DateTimeField(required=True)
    deleted_at = fields.DateTimeField(allow_none=True)

    class Meta:
        collection_name = 'Tenants'
