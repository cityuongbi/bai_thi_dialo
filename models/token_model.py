from pydantic import BaseModel


class TokenData(BaseModel):
    username: str
    email: str
    user_id: str
    permissions: list[str] = []
    is_super_admin: bool
    roles: list[str] = []
    tenant_id: str = None
